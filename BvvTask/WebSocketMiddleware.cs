﻿using BvvTask.Infrastructure;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace BvvTask
{
    public class WebSocketMiddleware
    {
        private RequestDelegate _Next;
        private WebSocketProcessor _Processor;

        public WebSocketMiddleware(RequestDelegate next, WebSocketProcessor processor)
        {
            _Next = next;
            _Processor = processor;
        }

        public async Task Invoke(HttpContext context)
        {
            if(context.Request.Path == "/ws" && context.WebSockets.IsWebSocketRequest)
            {
                var socket = await context.WebSockets.AcceptWebSocketAsync();
                await _Processor.Process(socket);
            }
            else
            {
                await _Next(context);
            }
        }
    }
}
