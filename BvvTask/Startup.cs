﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BvvTask.Infrastructure;
using BvvTask.Models;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json.Serialization;

namespace BvvTask
{
    public class Startup
    {
        private IConfiguration _Cfg;

        public Startup(IConfiguration cfg)
        {
            _Cfg = cfg;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc()
                .AddJsonOptions(options => options.SerializerSettings.ContractResolver = new DefaultContractResolver());
            //Текущая версия связана с MSSQL
            services.AddDbContext<AppDbContext>(options =>
            {
                if (Convert.ToBoolean(_Cfg["UsePostgreSQL"]))
                {
                    options.UseNpgsql(_Cfg["ConnectionStrings:PostgreSQL"]);
                }
                else
                {
                    options.UseSqlServer(_Cfg["ConnectionStrings:DefaultConnection"]);
                }
            });
            services.AddTransient<IRepository, Repository>();
            //Аутентификация на основе Cookie, на данных из Users
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
            .AddCookie(cfg =>
            {
                cfg.LoginPath = "/Auth/Login";
                cfg.AccessDeniedPath = "/Auth/AccessDenied";
            });
            services.AddSession();
            services.AddMemoryCache();
            services.AddTransient<WebSocketProcessor>();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseDeveloperExceptionPage();
            //Перехватим соединения по вебсокету
            app.UseWebSockets();
            app.UseMiddleware<WebSocketMiddleware>();
            app.UseStatusCodePages();
            app.UseStaticFiles();
            app.UseAuthentication();
            app.UseSession();
            app.UseMvc(builder =>
            {
                builder.MapRoute("", "{controller=Applied}/{action=Index}/{id?}");
            });
            InitialData.EnsurePopulated(app.ApplicationServices);
        }
    }
}
