﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BvvTask.Infrastructure
{
    public class WebSocketClient
    {
        public string ClientId { get; set; }
        public CancellationTokenSource CancellationSource { get; set; }
        public WebSocket Socket { get; set; }

        /// <summary>
        /// Задача при запуске в синглтон сервисе в привязке к переданному 
        /// на клиент уникальному идентификатору сохраняет токен отмены
        /// </summary>
        /// <returns></returns>
        public bool CancelCurrentTask()
        {
            if(CancellationSource != null)
            {
                CancellationSource.Cancel();
                CancellationSource = null;
                return true;
            }
            return false;
        }

        public async Task<bool> Send(object data)
        {
            if(Socket != null)
            {
                var message = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(data));
                await Socket.SendAsync(message, WebSocketMessageType.Text, true, CancellationToken.None);
                return true;
            }
            return false;
        }
    }
}
