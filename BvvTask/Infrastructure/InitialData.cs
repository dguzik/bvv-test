﻿using BvvTask.Models;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BvvTask.Infrastructure
{
    public class InitialData
    {
        public static void EnsurePopulated(IServiceProvider sp)
        {
            using (var scope = sp.CreateScope())
            {
                var repo = scope.ServiceProvider.GetRequiredService<IRepository>();
                if (!repo.Users.Any(u => u.Name == "Admin"))
                {
                    repo.AddUser(new User
                    {
                        Name = "Admin",
                        PasswordHash = "pw123"
                    });
                }
            }
        }
    }
}
