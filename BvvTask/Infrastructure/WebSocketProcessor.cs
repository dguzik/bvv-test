﻿using BvvTask.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BvvTask.Infrastructure
{
    public class WebSocketProcessor
    {
        public static ConcurrentDictionary<string, WebSocketClient> _Clients = new ConcurrentDictionary<string, WebSocketClient>();

        public WebSocketClient GetClient(string id)
        {
            if(!_Clients.TryGetValue(id, out WebSocketClient client))
            {
                client = new WebSocketClient { ClientId = id };
                if(_Clients.TryAdd(id, client))
                {
                    client = _Clients[id];
                }
            }
            return client;
        }

        public async Task Process(WebSocket socket)
        {
            byte[] buffer = new byte[1024 * 1024];
            var message = await socket.ReceiveAsync(buffer, CancellationToken.None);
            if (!socket.CloseStatus.HasValue)
            {
                //Получим "входной билет" от клиента
                var clientData = JsonConvert.DeserializeObject<SocketPayload>(Encoding.UTF8.GetString(buffer, 0, message.Count));
                if(!_Clients.TryGetValue(clientData.UserId, out WebSocketClient client))
                {
                    client = new WebSocketClient { ClientId = clientData.UserId };
                    if (!_Clients.TryAdd(clientData.UserId, client))
                        client = _Clients[clientData.UserId];
                }
                client.Socket = socket;
                //Далее из входящих сообщений можно получить только запрос отмены
                //из исходящих - текстовое оповещение (для алерта на клиенте) и текущий прогресс
                message = await socket.ReceiveAsync(buffer, CancellationToken.None);
                while (!socket.CloseStatus.HasValue)
                {
                    var request = JsonConvert.DeserializeObject<SocketPayload>(Encoding.UTF8.GetString(buffer, 0, message.Count));
                    switch (request.Request)
                    {
                        case "cancel":
                            if (client.CancelCurrentTask())
                            {
                                await client.Send(new WebSocketResult
                                {
                                    notificationText  = "#WebSocketProcessor: stopped"
                                });
                            }
                            break;
                    }
                    message = await socket.ReceiveAsync(buffer, CancellationToken.None);
                }
            }
        }
    }
}
