﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace BvvTask
{
    public class Program
    {
        public static void Main(string[] args)
        {
            BuildWebHost(args).Run();
        }

        public static IWebHost BuildWebHost(string[] args)
        {
            var cfg = new ConfigurationBuilder();
            var path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "../../../appsettings.json");
            if(!File.Exists(path))
                cfg.AddJsonFile("appsettings.json");
            else
                cfg.AddJsonFile(path);
            var builder = WebHost.CreateDefaultBuilder(args)
                .UseConfiguration(cfg.Build())
                .UseStartup<Startup>();
            return builder.Build();
        }
    }
}
