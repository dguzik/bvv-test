﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BvvTask.Models
{
    /// <summary>
    /// Аналог tbAgents
    /// </summary>
    public class Agent
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long AgentId { get; set; }
        public string Resources { get; set; }
        public string PrimaryObject { get; set; }
        public string SecondaryObject { get; set; }
        public string Data { get; set; }
    }
}
