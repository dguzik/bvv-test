﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BvvTask.Models
{
    public class LoginModel
    {
        [Required]
        public User User { get; set; }
        [UIHint("HiddenInput")]
        public string ReturnUrl { get; set; }
    }
}
