﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BvvTask.Models
{
    public class WebSocketResult
    {
        public string notificationText { get; set; }
        public bool completed { get; set; }
        public int progress { get; set; }
    }
}
