﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BvvTask.Models
{
    /// <summary>
    /// Посылка, отправляемая подключающимся клиентом
    /// и для запроса отмены серверной операции
    /// </summary>
    public class SocketPayload
    {
        public string Request { get; set; }
        public string UserId { get; set; }
    }
}
