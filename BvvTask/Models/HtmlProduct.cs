﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BvvTask.Models
{
    /// <summary>
    /// Аналог tbHtml
    /// </summary>
    public class HtmlProduct
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long HtmlProductId { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
        public decimal Price { get; set; }
        public int Amount { get; set; }
        public string Description { get; set; }
    }
}
