﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BvvTask.Models
{
    public class TaskParams
    {
        public string UserId { get; set; }
        public int RowsCount { get; set; }
    }
}
