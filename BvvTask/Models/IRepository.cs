﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BvvTask.Models
{
    public interface IRepository
    {
        IQueryable<User> Users { get; }
        void AddUser(User user);
        IQueryable<Agent> Agents { get; }
        IQueryable<HtmlProduct> Products { get; }
        void SaveAgent(Agent agent);
        void SaveProduct(HtmlProduct product);
        bool DeleteAgent(long id);
        bool DeleteProduct(long key);
    }
}
