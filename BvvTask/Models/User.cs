﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BvvTask.Models
{
    /// <summary>
    /// Аналог tbUser
    /// </summary>
    public class User
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long UserId { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        [UIHint("Password")]
        public string PasswordHash { get; set; }
        public List<Agent> Agents { get; set; }
    }
}
