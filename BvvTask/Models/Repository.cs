﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BvvTask.Models
{
    public class Repository : IRepository
    {
        private AppDbContext _Ctx;

        public Repository(AppDbContext ctx)
        {
            _Ctx = ctx;
        }

        public IQueryable<User> Users => _Ctx.Users;

        public IQueryable<Agent> Agents => _Ctx.Agents;

        public IQueryable<HtmlProduct> Products => _Ctx.Products;

        public void AddUser(User user)
        {
            _Ctx.Users.Add(user);
            _Ctx.SaveChanges();
        }

        public bool DeleteAgent(long id)
        {
            var agent = _Ctx.Agents.FirstOrDefault(a => a.AgentId == id);
            if(agent != null)
            {
                _Ctx.Remove(agent);
                _Ctx.SaveChanges();
                return true;
            }
            return false;
        }

        public bool DeleteProduct(long key)
        {
            var product = _Ctx.Products.FirstOrDefault(p => p.HtmlProductId == key);
            if (product != null)
            {
                _Ctx.Products.Remove(product);
                _Ctx.SaveChanges();
                return true;
            }
            return false;
        }

        public void SaveAgent(Agent agent)
        {
            if(agent.AgentId == 0)
            {
                _Ctx.Agents.Add(agent);
            }
            else
            {
                _Ctx.Entry(agent).State = EntityState.Modified;
            }
            _Ctx.SaveChanges();
        }

        public void SaveProduct(HtmlProduct product)
        {
            if(product.HtmlProductId == 0)
            {
                _Ctx.Products.Add(product);
            }
            else
            {
                _Ctx.Entry(product).State = EntityState.Modified;
            }
            _Ctx.SaveChanges();
        }
    }
}
