﻿using BvvTask.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace BvvTask.Controllers
{
    public class AuthController: Controller
    {
        private IRepository _Repo;

        public AuthController(IRepository repo)
        {
            _Repo = repo;
        }

        public IActionResult Login(string returnUrl = "/")
        {
            return View(new LoginModel
            {
                ReturnUrl = returnUrl,
                User = new Models.User()
            });
        }

        [HttpPost]
        public async Task<IActionResult> Login(LoginModel model, string returnUrl = "/")
        {
            if (ModelState.IsValid)
            {
                var user = _Repo.Users.FirstOrDefault(u => u.Name == model.User.Name);
                if (user != null)
                {
                    if (model.User.PasswordHash == user.PasswordHash)
                    {
                        var principal = new ClaimsPrincipal();
                        var claimsIdentity = new ClaimsIdentity(new[] { new Claim(ClaimTypes.Name, model.User.Name) }, 
                            CookieAuthenticationDefaults.AuthenticationScheme);
                        principal.AddIdentity(claimsIdentity);
                        await HttpContext.SignInAsync(principal);
                        return Redirect(returnUrl);
                    }
                    else
                    {
                        ModelState.AddModelError(nameof(model.User.Name), $"Wrong password for the user {model.User.Name}");
                    }
                }
                else
                {
                    ModelState.AddModelError(nameof(model.User.Name), $"There is no user {model.User.Name} in the database");
                }
            }
            return View(model);
        }

        public async Task<IActionResult> Logout(string returnUrl = "/")
        {
            await HttpContext.SignOutAsync();
            return Redirect(returnUrl);
        }

        public string AccessDenied()
        {
            return "Access denied";
        }
    }
}
