﻿using BvvTask.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BvvTask.Controllers
{
    /// <summary>
    /// Редактируемое HTML-поле
    /// </summary>
    [Authorize]
    public class HtmlTableController: Controller
    {
        private IRepository _Repository;

        public HtmlTableController(IRepository repo)
        {
            _Repository = repo;
        }

        public IActionResult Index() => View();

        [Route("/Rows")]
        [HttpGet]
        public IEnumerable<HtmlProduct> Get()
        {
            return _Repository.Products.ToList();
        }

        [Route("/Row")]
        [HttpPost]
        public HtmlProduct Post(string values)
        {
            var product = new HtmlProduct();
            JsonConvert.PopulateObject(values, product);
            _Repository.SaveProduct(product);
            return product;
        }

        [Route("/Row")]
        [HttpPut]
        public HtmlProduct Put(long key, string values)
        {
            var product = _Repository.Products.First(p => p.HtmlProductId == key);
            JsonConvert.PopulateObject(values, product);
            _Repository.SaveProduct(product);
            return product;
        }

        [Route("/Row")]
        [HttpDelete]
        public bool Delete(long key)
        {
            return _Repository.DeleteProduct(key);
        }
    }
}
