﻿using BvvTask.Infrastructure;
using BvvTask.Models;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BvvTask.Controllers
{
    /// <summary>
    /// Контроллер запросов к аналогу таблицы tbAgents
    /// </summary>
    [Authorize]
    public class AppliedController: Controller
    {
        private WebSocketProcessor _SocketProcessor;
        private IRepository _Repository;

        public AppliedController(WebSocketProcessor processor, IRepository repo)
        {
            _SocketProcessor = processor;
            _Repository = repo;
        }

        public IActionResult Index() => View("Index", Guid.NewGuid().ToString());

        /// <summary>
        /// Запустим имитацию бурной деятельности
        /// статус будем отслеживать асинхронно на 
        /// клиенте, получая сообщения по WebSocket
        /// </summary>
        /// <param name="prms"></param>
        /// <returns></returns>
        public IActionResult PerformTask([FromBody] TaskParams prms)
        {
            var client = _SocketProcessor.GetClient(prms.UserId);
            CancellationTokenSource cancellationSource = new CancellationTokenSource();
            client.CancellationSource = cancellationSource;
            var token = cancellationSource.Token;
            // Это наша вставка строк, мы ее будем отслеживать по WebSocket
            var scope = HttpContext.RequestServices.CreateScope();
            Task.Run(async () =>
            {
                using (scope)
                {
                    var repository = scope.ServiceProvider.GetRequiredService<IRepository>();
                    var rand = new Random();
                    for (int i = 0; i < prms.RowsCount; i++)
                    {
                        if (token.IsCancellationRequested)
                            break;
                        await client.Send(new WebSocketResult
                        {
                            completed = false,
                            progress = (int)((float)i / prms.RowsCount * 100)
                        });
                        var agent = new Agent();
                        agent.Data = rand.Next().ToString("X2");
                        agent.PrimaryObject = rand.Next().ToString("X2");
                        agent.SecondaryObject = rand.Next().ToString("X2");
                        agent.Resources = rand.Next().ToString("X2");
                        repository.SaveAgent(agent);
                        //Мы же не будем сохранять 1 секунду, продлим задачу
                        await Task.Delay(1000);
                    }
                }
                if (!token.IsCancellationRequested)
                {
                    await client.Send(new WebSocketResult
                    {
                        completed = true,
                        progress = 100
                    });
                }
            }, token);
            return Json(new { started = true });
        }

        [Route("agents")]
        [HttpGet]
        public IEnumerable<Agent> Get()
        {
            return _Repository.Agents.ToList();
        }

        [Route("agent")]
        [HttpPost]
        public Agent Post(string values)
        {
            var agent = new Agent();
            JsonConvert.PopulateObject(values, agent);
            _Repository.SaveAgent(agent);
            return agent;
        }

        [Route("agent")]
        [HttpPut]
        public Agent Put(long key, string values)
        {
            var agent = _Repository.Agents.First(a => a.AgentId == key);
            JsonConvert.PopulateObject(values, agent);
            _Repository.SaveAgent(agent);
            return agent;
        }

        [Route("agent")]
        [HttpDelete]
        public bool Delete(long key)
        {
            return _Repository.DeleteAgent(key);
        }
    }
}
